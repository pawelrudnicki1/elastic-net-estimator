import pandas as pd
import numpy as np
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.linear_model import ElasticNet

df = pd.read_csv('base_with_values.csv')
df = df.replace(-200, np.NaN)
index = 0
for x in range(0, 200, 10):
    chunked_df = df[index:index+11]
    chunked_df[:] = IterativeImputer(estimator=ElasticNet(normalize=True, precompute=True, warm_start=True, selection='random', l1_ratio=0.00000001), skip_complete=True).fit_transform(chunked_df)
    if index == 0: chunked_df.to_csv("first_values.csv");
    df.append(chunked_df, ignore_index=True)
    index += 10;

